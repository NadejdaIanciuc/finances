import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoaderComponent } from "./components/loader/loader.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { CommonModule } from "@angular/common";

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        NgxChartsModule,
    ],
    declarations: [
        LoaderComponent
    ],
    exports: [
        ReactiveFormsModule,
        FormsModule,
        NgxChartsModule,
        LoaderComponent
    ]
})
export class SharedModule { }

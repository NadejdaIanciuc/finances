import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app-routing.module';
import { UsersService } from './shared/services/users.service';
import { AuthService } from './shared/services/auth.service';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { AuthGuard } from './shared/services/auth.guard';

@NgModule({
    declarations: [
        AppComponent, 
        NotFoundComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AuthModule,
        AppRoutingModule,
        BrowserAnimationsModule,
    ],
    providers: [
        UsersService, 
        AuthService, 
        AuthGuard
    ],
    bootstrap: [
        AppComponent
    ],
})
export class AppModule {}

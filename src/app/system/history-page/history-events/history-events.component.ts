import { Component, Input, OnInit } from '@angular/core';
import { WFMEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';

@Component({
    selector: 'wfm-history-events',
    templateUrl: './history-events.component.html',
    styleUrls: ['./history-events.component.scss'],
})
export class HistoryEventsComponent implements OnInit {
    @Input() categories: Category[] = [];
    @Input() events: WFMEvent[] = [];
    searchValue = '';
    searchPlaceholder = 'Сумма';
    searchField = 'amount';

    constructor() {}

    ngOnInit() {
        this.events.forEach((e) => {
            const category = this.categories.find((c) => c.id === e.category);
            e.catName = category ? category.name : 'Unknown';
        });
    }

    getEventClass(e: WFMEvent) {
        return {
            label: true,
            'label-danger': e.type === 'outcome',
            'label-success': e.type === 'income',
        };
    }

    changeCriteria(field: string) {
        const namesMap: { [key: string]: string } = {
            amount: 'Сумма',
            date: 'Дата',
            category: 'Категория',
            type: 'Тип',
        };
        this.searchPlaceholder = namesMap[field];
        this.searchField = field;
    }
}

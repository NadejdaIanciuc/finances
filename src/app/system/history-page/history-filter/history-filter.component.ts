import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Category } from '../../shared/models/category.model';

interface FilterData {
    types: string[];
    categories: string[];
    period: string;
}

@Component({
    selector: 'wfm-history-filter',
    templateUrl: './history-filter.component.html',
    styleUrls: ['./history-filter.component.scss'],
})
export class HistoryFilterComponent {
    @Output() onFilterCancel = new EventEmitter<void>();
    @Output() onFilterApply = new EventEmitter<FilterData>();

    @Input() categories: Category[] = [];

    selectedPeriod = 'd';
    selectedTypes: string[] = [];
    selectedCategories: string[] = [];

    timePeriods = [
        { type: 'd', label: 'День' },
        { type: 'w', label: 'Неделя' },
        { type: 'M', label: 'Месяц' },
    ];

    types = [
        { type: 'income', label: 'Доход' },
        { type: 'outcome', label: 'Расход' },
    ];

    closeFilter(): void {
        this.selectedTypes = [];
        this.selectedCategories = [];
        this.selectedPeriod = 'd';
        this.onFilterCancel.emit();
    }

    private calculateInputParams(field: string[], checked: boolean, value: string): void {
        if (checked) {
            if (field.indexOf(value) === -1) {
                field.push(value);
            }
        } else {
            const index = field.indexOf(value);
            if (index !== -1) {
                field.splice(index, 1);
            }
        }
    }

    handleChangeType(event: Event): void {
        const inputElement = event.target as HTMLInputElement;
        if (inputElement) {
            this.calculateInputParams(this.selectedTypes, inputElement.checked, inputElement.value);
        }
    }

    handleChangeCategory(event: Event): void {
        const inputElement = event.target as HTMLInputElement;
        if (inputElement) {
            this.calculateInputParams(this.selectedCategories, inputElement.checked, inputElement.value);
        }
    }

    applyFilter(): void {
        this.onFilterApply.emit({
            types: this.selectedTypes,
            categories: this.selectedCategories,
            period: this.selectedPeriod,
        });
    }
}

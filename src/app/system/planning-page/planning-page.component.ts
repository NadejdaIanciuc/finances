import { Component, OnDestroy, OnInit } from '@angular/core';
import { CategoriesService } from '../shared/services/categories.service';
import { EventsService } from '../shared/services/events.service';
import { Category } from '../shared/models/category.model';
import { WFMEvent } from '../shared/models/event.model';
import { Subscription, combineLatest } from 'rxjs';

@Component({
    selector: 'wfm-planning-page',
    templateUrl: './planning-page.component.html',
    styleUrls: ['./planning-page.component.scss'],
})
export class PlanningPageComponent implements OnInit, OnDestroy {
    isLoaded = false;
    s1!: Subscription;
    categories: Category[] = [];
    events: WFMEvent[] = [];
    totalIncome = 0;
    totalExpenses = 0;

    constructor(
        private categoriesService: CategoriesService,
        private eventsService: EventsService
    ) {}

    ngOnInit() {
        this.s1 = combineLatest([
            this.categoriesService.getCategories(),
            this.eventsService.getEvents(),
        ]).subscribe((data: [Category[], WFMEvent[]]) => {
            this.categories = data[0];
            this.events = data[1];

            this.calculateTotalIncome();
            this.calculateTotalExpenses();

            this.isLoaded = true;

            this.updateProgressBars();
        });
    }

    calculateTotalIncome() {
        this.totalIncome = 0;
        for (const event of this.events) {
            if (event.type === 'income') {
                this.totalIncome += event.amount;
            }
        }
        console.log('Total Income:', this.totalIncome);
    }

    calculateTotalExpenses() {
        this.totalExpenses = 0;
        for (const event of this.events) {
            if (event.type === 'outcome') {
                this.totalExpenses += event.amount;
            }
        }
        console.log('Total Expenses:', this.totalExpenses);
    }

    updateProgressBars() {
        this.categories.forEach((category) => {
            const progressBar = document.getElementById(
                `progress-bar-${category.id}`
            ) as HTMLElement;
            if (progressBar) {
                const percent = this.getPercent(category);
                progressBar.style.width = `${percent}%`;
                progressBar.setAttribute('aria-valuenow', `${percent}`);
            }
        });
    }

    getPercent(cat: Category): number {
        const percent = (100 * this.getCategoryCost(cat)) / cat.capacity;
        return percent > 100 ? 100 : percent;
    }

    getCategoryCost(cat: Category): number {
        const catEvents = this.events.filter(
            (e) => e.category === cat.id && e.type === 'outcome'
        );
        return catEvents.reduce((total, e) => {
            total += e.amount;
            return total;
        }, 0);
    }

    getCatPercent(cat: Category): string {
        return this.getPercent(cat) + '%';
    }

    getCatColorClass(cat: Category): string {
        const percent = this.getPercent(cat);
        return percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
    }

    ngOnDestroy() {
        if (this.s1) {
            this.s1.unsubscribe();
        }
    }
}

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';

import { Category } from '../../shared/models/category.model';
import { WFMEvent } from '../../shared/models/event.model';
import { EventsService } from '../../shared/services/events.service';
import { Subscription } from 'rxjs';
import { Message } from 'src/app/shared/models/message.modal';

@Component({
    selector: 'wfm-add-event',
    templateUrl: './add-event.component.html',
    styleUrls: ['./add-event.component.scss'],
})
export class AddEventComponent implements OnInit, OnDestroy {
    sub1!: Subscription;
    sub2!: Subscription;
    totalIncome: number = 0;
    totalExpenses: number = 0;
    @Input() categories: Category[] = [];
    @Input() events: WFMEvent[] = [];

    types = [
        { type: 'income', label: 'Доход' },
        { type: 'outcome', label: 'Расход' },
    ];

    message!: Message;

    constructor(private eventsService: EventsService) {}

    ngOnInit() {
        this.message = new Message('danger', '');
        this.calculateTotalIncomeAndExpenses();
    }

    private calculateTotalIncomeAndExpenses() {
        this.totalIncome = this.events
            .filter((event) => event.type === 'income')
            .reduce((total, event) => total + event.amount, 0);
    
        this.totalExpenses = this.events
            .filter((event) => event.type === 'outcome')
            .reduce((total, event) => total + event.amount, 0);
    }

    private showMessage(text: string, type: string) {
        this.message.text = text;
        this.message.type = type;
        window.setTimeout(() => {
            this.message.text = '';
            this.message.type = 'danger'; // Reset message type to default
        }, 5000);
    }

    onSubmit(form: NgForm) {
        let { amount, description, category, type } = form.value;
        if (amount < 0) amount *= -1;
    
        const event = new WFMEvent(
            type,
            amount,
            +category,
            moment().format('DD.MM.YYYY HH:mm:ss'),
            description
        );
    
        if (type === 'income') {
            this.showMessage('Доход успешно добавлен', 'success');
        } else {
            this.totalIncome -= amount;
            this.calculateTotalIncomeAndExpenses();
    
            if (amount > this.totalIncome) {
                this.showMessage(
                    `На счету недостаточно средств. Вам нехватает ${
                        amount - this.totalIncome
                    }`,
                    'danger'
                );
                return;
            }
        }
    
        this.sub2 = this.eventsService.addEvent(event).subscribe(() => {
            form.setValue({
                amount: 0,
                description: ' ',
                category: 1,
                type: 'outcome',
            });
            this.calculateTotalIncomeAndExpenses();
        });
    }

    ngOnDestroy() {
        if (this.sub1) this.sub1.unsubscribe();
        if (this.sub2) this.sub2.unsubscribe();
    }
}
